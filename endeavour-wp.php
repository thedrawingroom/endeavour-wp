<?php

/**
 * Plugin Name: Endeavour WP
 * Plugin URI: https://bitbucket.org/thedrawingroom/endeavour-wp
 * Description: Adds common TDR customisations to WordPress
 * Version: 1.0.0
 * Author: The Drawing Room
 * Author URI: https://www.thedrawingroomcreative.com/
 */

require_once(__DIR__ . "/src/wp-functions.php");
