<?php
/**
 * Register ACF Options pages
 *
 * @link http://www.advancedcustomfields.com/resources/acf_add_options_page/
 */

    function endeavour_acf_init() {
        if (function_exists('acf_add_options_page')) {

            acf_add_options_page();
            acf_add_options_sub_page('Header');
            acf_add_options_sub_page('Footer');
            acf_add_options_sub_page('Global');
            acf_add_options_sub_page('Developer');
        }

        if (function_exists('acf_set_options_page_title')) {
            acf_set_options_page_title(__('Site Options'));
        }
    }

    add_action('acf/init', 'endeavour_acf_init');

/**
 * This function creates the 'format' box on the Wordpress WYSIWYG's (tiny_mce)
 * It allows you to let custom classes be added on content from the CMS
 *
 * @link https://codex.wordpress.org/Plugin_API/Filter_Reference/tiny_mce_before_init
 * @link http://www.tinymce.com/wiki.php/Configuration:formats
 * @link http://www.wpbeginner.com/wp-tutorials/how-to-add-custom-styles-to-wordpress-visual-editor/
 *
 */

    // Add format box

        function wpb_mce_buttons_2($buttons) {
        	array_unshift($buttons, 'styleselect');
        	return $buttons;
        }
        add_filter('mce_buttons_2', 'wpb_mce_buttons_2');

    // Define the style_formats array

        function my_mce_insert_format_before_init($init_array) {

        	$style_formats = array(
        		// Each array child is a format with it's own settings
        		array(
        			'title' => 'Colour Primary',
        			'inline' => 'span',
        			'classes' => 'color-primary',
        			'wrapper' => true,
        		),
        		array(
        			'title' => 'Colour Secondary',
        			'inline' => 'span',
        			'classes' => 'color-secondary',
        			'wrapper' => true,
        		),
        		array(
        			'title' => 'Colour Tertiary',
        			'inline' => 'span',
        			'classes' => 'color-tertiary',
        			'wrapper' => true,
        		),
        		array(
        			'title' => 'Colour Light Grey',
        			'inline' => 'span',
        			'classes' => 'color-light-grey',
        			'wrapper' => true,
        		),
        		array(
        			'title' => 'Colour Mid Grey',
        			'inline' => 'span',
        			'classes' => 'color-mid-grey',
        			'wrapper' => true,
        		),
        		array(
        			'title' => 'Colour Dark Grey',
        			'inline' => 'span',
        			'classes' => 'color-dark-grey',
        			'wrapper' => true,
        		),
        		array(
        			'title' => 'Colour White',
        			'inline' => 'span',
        			'classes' => 'color-white',
        			'wrapper' => true,
        		),
        		array(
        			'title' => 'Heading 1',
        			'inline' => 'span',
        			'classes' => 'h1',
        			'wrapper' => false,
        		),
        		array(
        			'title' => 'Heading 2',
        			'inline' => 'span',
        			'classes' => 'h2',
        			'wrapper' => false,
        		),
        		array(
        			'title' => 'Heading 3',
        			'inline' => 'span',
        			'classes' => 'h3',
        			'wrapper' => false,
        		),
        		array(
        			'title' => 'Heading 4',
        			'inline' => 'span',
        			'classes' => 'h4',
        			'wrapper' => false,
        		),
                array(
        			'title' => 'Heading 5',
        			'inline' => 'span',
        			'classes' => 'h5',
        			'wrapper' => false,
        		),
        		array(
        			'title' => 'Text Uppercase',
        			'inline' => 'span',
        			'classes' => 'text-uppercase',
        			'wrapper' => false,
        		),
        		array(
        			'title' => 'Text Light',
        			'inline' => 'span',
        			'classes' => 'text-light',
        			'wrapper' => false,
        		),
        		array(
        			'title' => 'Text Bold',
        			'inline' => 'span',
        			'classes' => 'text-bold',
        			'wrapper' => false,
        		),
        		array(
        			'title' => 'Text Underline',
        			'inline' => 'span',
        			'classes' => 'text-underline',
        			'wrapper' => false,
        		),
        		array(
        			'title' => 'Text Small',
        			'inline' => 'span',
        			'classes' => 'text-small',
        			'wrapper' => false,
        		),
                array(
        			'title' => 'Custom Bullets',
        			'selector' => 'ul',
        			'classes' => 'custom-bullets',
        			'wrapper' => false,
        		),
        	);
        	// Insert the array, JSON ENCODED, into 'style_formats'
        	$init_array['style_formats'] = json_encode($style_formats);

        	return $init_array;
        }
        // Attach callback to 'tiny_mce_before_init'
        add_filter('tiny_mce_before_init', 'my_mce_insert_format_before_init');

/**
 * Wrap videos in a responsive div
 *
 * @param string $html The video HTML
 * @return string The new html
 */

     function tdr_embed_oembed_html($html) {
     	$html = str_replace('<iframe', '<iframe class="embed-responsive-item"', $html);
     	return '<div class="embed-responsive embed-responsive-16by9 width-100">' . $html . '</div>';
     }
     add_filter('embed_oembed_html', 'tdr_embed_oembed_html', 99, 4);

 /**
  *
  * Get Child Pages
  *
  * Gets the child pages of the $post->ID that is passed
  *
  */

     function tdr_get_sub_pages($page_id, $field = 'child_of', $hierarchical = true) {

     	$args = array(
     		'sort_order' => 'ASC',
     		'sort_column' => 'menu_order',
     		'hierarchical' => $hierarchical,
     		$field => $page_id,
     		'post_type' => 'page',
     		'post_status' => 'publish'
     	);

     	$pages = get_pages($args);

     	return $pages;
     }

 /**
  * Clean up the_excerpt()
  */

     function excerpt_more() {
     	return '...';
     }
     add_filter('excerpt_more', 'excerpt_more');

     function custom_excerpt_length($length) {
     	return 20;
     }
     add_filter('excerpt_length', 'custom_excerpt_length', 999);

/**
 * Custom Excerpt outside loop
 * @return string
 */

	function get_excerpt_outside_loop($postID) {

		// Get post by the passed ID
		$current_post = get_post($postID);

		// Setup current $post object
		setup_postdata($current_post);

		// Run default excerpt function on current post
		$excerpt = get_the_excerpt();

		// Return excerpt
		if ($excerpt) {
			return $excerpt;
		}
	}
